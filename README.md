# SQLAlchemy Mutable

[![Documentation Status](https://readthedocs.org/projects/dsbowen-sqlalchemy-mutable/badge/?version=latest)](https://dsbowen-sqlalchemy-mutable.readthedocs.io/en/latest/?badge=latest)
[![pipeline status](https://gitlab.com/dsbowen/sqlalchemy-mutable/badges/master/pipeline.svg)](https://gitlab.com/dsbowen/sqlalchemy-mutable/-/commits/master)
[![coverage report](https://gitlab.com/dsbowen/sqlalchemy-mutable/badges/master/coverage.svg)](https://gitlab.com/dsbowen/sqlalchemy-mutable/-/commits/master)
[![PyPI version](https://badge.fury.io/py/sqlalchemy-mutable.svg)](https://badge.fury.io/py/sqlalchemy-mutable)
[![License](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://gitlab.com/dsbowen/sqlalchemy-mutable/-/blob/master/LICENSE)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/dsbowen%2Fsqlalchemy-mutable/HEAD?urlpath=lab/tree/examples)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

SQLAlchemy Mutable provided utilities for creating flexible and powerful database columns for use with SQLAlchemy.

To test with SQLAlchemy:

```
$ make test
```

or 

```
$ tox
```

To test with Flask-SQLAlchemy:

```
$ tox -e flask
```

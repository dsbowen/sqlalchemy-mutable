Changelog
=========

1.0.2
-----

- Stopped wrapping immutable objects in the ``Mutable`` class. There's no need to track changes to immutable objects.
- Changed ``HTMLAttributes`` to ``HTMLSettings`` with more functionality. HTML settings maps HTML tag names to attributes dictionaries. HTML attributes maps attribute names to values.
- Added ``utils.is_callable``.

1.0.1
-----

I changed the fundamental structure of SQLAlchemy Mutable from an inheritance-based structure to a composition-based structure. Instead of forcing objects to inherit ``Mutable``, I wrap them in a ``Mutable`` instance. The object you care about is stored in the ``_object`` attribute. This allows you to store any pickle-able object in a mutable column and automatically track changes without any additional work.

0.0.1
-----

- First release on PyPI
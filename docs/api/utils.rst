sqlalchemy\_mutable.utils
=========================

.. automodule:: sqlalchemy_mutable.utils
   :members:
   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      is_instance
      is_subclass
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      partial
   
   

   
   
   




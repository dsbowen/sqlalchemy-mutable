API reference
=============

.. toctree::
    :maxdepth: 2

    Mutable class <mutable>
    Column types <types>
    HTML utilities <html_settings>
    Utilities <utils>
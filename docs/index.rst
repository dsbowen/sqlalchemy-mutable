.. SQLAlchemy Mutable documentation master file, created by
   sphinx-quickstart on Mon Nov 12 14:17:27 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SQLAlchemy Mutable documentation
================================

SQLAlchemy Mutable provided utilities for creating flexible and powerful database columns for use with `SQLAlchemy <https://www.sqlalchemy.org/>`_.

.. image:: https://readthedocs.org/projects/dsbowen-sqlalchemy-mutable/badge/?version=latest
   :target: https://dsbowen-sqlalchemy-mutable.readthedocs.io/en/latest/?badge=latest
.. image:: https://gitlab.com/dsbowen/sqlalchemy-mutable/badges/master/pipeline.svg
   :target: https://gitlab.com/dsbowen/sqlalchemy-mutable/-/commits/master
.. image:: https://gitlab.com/dsbowen/sqlalchemy-mutable/badges/master/coverage.svg
   :target: https://gitlab.com/dsbowen/sqlalchemy-mutable/-/commits/master
.. image:: https://badge.fury.io/py/sqlalchemy-mutable.svg
   :target: https://badge.fury.io/py/sqlalchemy-mutable
.. image:: https://img.shields.io/badge/License-MIT-brightgreen.svg
   :target: https://gitlab.com/dsbowen/sqlalchemy-mutable/-/blob/master/LICENSE
.. image:: https://mybinder.org/badge_logo.svg
   :target: https://mybinder.org/v2/gl/dsbowen%2Fsqlalchemy-mutable/HEAD?urlpath=lab
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black

|
Installation
============

Install the latest stable build.

.. code-block::

   $ pip install sqlalchemy-mutable

Install the latest dev build.

.. code-block::

   $ pip install git+https://gitlab.com/dsbowen/sqlalchemy-mutable.git

Quickstart
==========

Let's start by setting up a database model with a mutable column type.

.. code-block:: python

   >>> from sqlalchemy import create_engine, Column, Integer
   >>> from sqlalchemy.orm import declarative_base, sessionmaker, scoped_session
   >>> from sqlalchemy_mutable.types import MutablePickleType
   >>> engine = create_engine('sqlite:///:memory:')
   >>> session_factory = sessionmaker(bind=engine)
   >>> Session = scoped_session(session_factory)
   >>> session = Session()
   >>> Base = declarative_base()
   >>> class MyModel(Base):
   ...     __tablename__ = "mymodel"
   ...     id = Column(Integer, primary_key=True)
   ...     mutable = Column(MutablePickleType)
   >>> Base.metadata.create_all(engine)

We can see how this handles nested change tracking:

.. code-block:: python

   >>> class MyClass:
   ...     def __init__(self, greeting):
   ...         self.greeting = greeting
   >>> model = MyModel()
   >>> model.mutable = MyClass("Hello, world!")
   >>> print(model.mutable.greeting)
   "Hello, world!"
   >>> session.add(model)
   >>> session.commit()
   >>> model.mutable.greeting = "Hello, moon!"
   >>> session.commit()
   >>> print(model.mutable.greeting)
   "Hello, moon!"


Contents
========

.. toctree::
   :maxdepth: 2

   api/index
   examples/index
   Changelog <changelog>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Citations
=========

.. code-block::

   @software(bowen2021sqlalchemy-mutable,
      title={ SQLAlchemy Mutable },
      author={ Bowen, Dillon },
      year={ 2021 },
      url={ https://dsbowen.gitlab.io/sqlalchemy-mutable }
   )

Acknowledgements
================

Original inspiration drawn from `SQLAlchemy JSON <https://pypi.org/project/sqlalchemy-json/>`_.
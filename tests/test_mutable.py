import copy
import math
import operator
import os
import sys
from itertools import product

import pytest

from sqlalchemy_mutable import Mutable

if os.environ.get("FLASK_SETUP") == "1":
    from flask_setup import JSONModel, PickleModel as Model, db

    session = db.session
else:
    from setup import JSONModel, PickleModel as Model, session

# skip long-running tests
SKIP_DUNDER_METHODS = False
SKIP_FUNCTIONS = False
SKIP_TWO_ARG_FUNCTIONS = False
SKIP_FUNCTIONS_BOTH_MUTABLE = False
SKIP_INPLACE_FUNCTIONS = False


class Person:
    def __init__(self, name, friend=None):
        self.name = name
        self.friend = friend


def test_mutable_setting():
    # test that you can set the mutable attribute
    model = Model()
    model.mutable = Person("World")
    assert model.mutable.name == "World"
    assert model.mutable.root == model.mutable


@pytest.mark.parametrize("model_cls", [Model, JSONModel])
def test_none_setting(model_cls):
    # test that you can set the mutable attribute to None
    model = model_cls()
    session.add(model)
    session.commit()
    assert model.mutable is None

    model.mutable = None
    session.commit()
    assert model.mutable is None


def test_attribute_tracking():
    # test basic attribute tracking
    model = Model()
    model.mutable = Person("World")
    session.add(model)
    session.commit()
    model.mutable.name = "Moon"
    session.commit()
    assert model.mutable.name == "Moon"
    assert model.mutable._tracked_attr_names == {"name", "friend"}


def test_nested_attribute_tracking():
    model = Model()
    model.mutable = Person("World")
    model.mutable.friend = Person("Moon")
    session.add(model)
    session.commit()
    model.mutable.friend.name = "Sun"
    session.commit()
    assert model.mutable.friend.name == "Sun"
    assert model.mutable.friend.root == model.mutable
    assert model.mutable.friend._tracked_attr_names == {"name", "friend"}


def test_previously_set_attributes_tracking():
    # test that attributes originally set before converting the object to a mutable are
    # tracked
    person = Person("World")
    person.friend = Person("Moon")
    model = Model()
    model.mutable = person
    session.add(model)
    session.commit()
    model.mutable.friend.name = "Sun"
    session.commit()
    assert model.mutable.friend.name == "Sun"
    assert model.mutable.friend.root == model.mutable
    assert model.mutable.friend._tracked_attr_names == {"name", "friend"}


def test_circular_attribute_tracking():
    # test that you can track objects with circular relationships
    world = Mutable(Person("World"))
    moon = Mutable(Person("Moon"))
    model = Model()
    model.mutable = world

    world.friend = moon
    assert world.friend.name == "Moon"
    assert world.root == world

    moon.friend = world
    assert moon.friend.name == "World"
    assert moon.root == world

    session.add(model)
    session.commit()
    world, moon = model.mutable, model.mutable.friend
    assert world.name == "World"
    assert world.friend.name == "Moon"
    assert world.root == world

    assert moon.name == "Moon"
    assert moon.friend.name == "World"
    assert moon.root == world


def test_underlying_type_compatibility():
    # test that you can't set new attributes if incompatible with the object's type
    model = Model()
    model.mutable = "Hello, world!"
    with pytest.raises(AttributeError, match=r"'str' object has no attribute *"):
        model.mutable.attribute = "value"


def test_del_attribute():
    # test that you can delete an attribute
    model = Model()
    model.mutable = Person("World")
    model.mutable.friend = Person("Moon")
    session.add(model)
    session.commit()
    del model.mutable.friend.name
    session.commit()
    assert not hasattr(model.mutable.friend, "name")


values = [
    None,
    -1,
    0,
    1,
    1.5,
    complex(1, 2),
    True,
    False,
    "Hello, world",
    {0, 1, 2},
    (0, 1, 2),
    [0, 1, 2],
    {"Hello": "World"},
]
dunder_methods = [
    "__pos__",
    "__neg__",
    "__invert__",
    "__long__",
    "__nonzero__",
]


@pytest.mark.skipif(SKIP_DUNDER_METHODS, reason="skipping dunder methods")
@pytest.mark.parametrize("value, method", product(values, dunder_methods))
def test_dunder_methods(value, method):
    # test overloaded dunder methods
    model = Model()
    model.mutable = value
    session.add(model)
    session.commit()

    expected_error = error = None
    try:
        expected_result = getattr(value, method)()
    except:
        expected_error = sys.exc_info()
    try:
        result = getattr(model.mutable, method)()
    except:
        error = sys.exc_info()

    if expected_error is error is None:
        assert expected_result == result
    else:
        assert expected_error is not None and error is not None


functions = [
    abs,
    round,
    math.floor,
    math.ceil,
    math.trunc,
    int,
    float,
    complex,
    bool,
    hash,
    len,
    copy.copy,
    copy.deepcopy,
]


@pytest.mark.skipif(SKIP_FUNCTIONS, reason="skipping functions")
@pytest.mark.parametrize("value, func", product(values, functions))
def test_functions(value, func):
    # test common functions that require one argument where the argument is a mutable object
    model = Model()
    model.mutable = value
    session.add(model)
    session.commit()

    expected_error = error = None
    try:
        expected_result = func(value)
    except:
        expected_error = sys.exc_info()
    try:
        result = func(model.mutable)
    except:
        error = sys.exc_info()

    assert (expected_error is error is None) or (
        expected_error is not None and error is not None
    )
    if expected_error is None:
        assert expected_result == result


def add(x, y):
    return x + y


def sub(x, y):
    return x - y


def mul(x, y):
    return x * y


def floordiv(x, y):
    return x // y


def div(x, y):
    return x / y


def mod(x, y):
    return x % y


def pow(x, y):
    return x ** y


def and_(x, y):
    return x & y


def or_(x, y):
    return x | y


def xor(x, y):
    return x ^ y


functions_with_other = [
    operator.eq,
    operator.ne,
    operator.lt,
    operator.le,
    operator.gt,
    operator.ge,
    add,
    sub,
    mul,
    floordiv,
    div,
    mod,
    pow,
    and_,
    or_,
    xor,
]


@pytest.mark.skipif(SKIP_TWO_ARG_FUNCTIONS, reason="skipping two argument functions")
@pytest.mark.parametrize(
    "value, other, func, left_argument",
    product(values, values, functions_with_other, (True, False)),
)
def test_two_arg_functions(value, other, func, left_argument):
    # test functions that require two arguments where one argument is a mutable object
    if func == mod and isinstance(other, str) and not left_argument:
        pytest.skip(
            "__mod__ is implemented for str, so Mutable's __rmod__ is never called"
        )

    model = Model()
    model.mutable = value
    session.add(model)
    session.commit()

    expected_error = error = None
    try:
        expected_result = func(value, other) if left_argument else func(other, value)
    except:
        expected_error = sys.exc_info()
    try:
        result = (
            func(model.mutable, other) if left_argument else func(other, model.mutable)
        )
    except:
        error = sys.exc_info()
    if expected_error is error is None:
        assert expected_result == result
    else:
        assert expected_error is not None and error is not None


@pytest.mark.skipif(
    SKIP_FUNCTIONS_BOTH_MUTABLE, reason="skipping functions both mutable"
)
@pytest.mark.parametrize(
    "value, other, func", product(values, values, functions_with_other)
)
def test_functions_both_mutable(value, other, func):
    # test common functions that require two arguments where both arguments are mutable objects
    if func == mod and isinstance(value, str):
        pytest.skip(
            "__mod__ is implemented for str, so Mutable's __rmod__ is never called"
        )

    model0 = Model()
    model0.mutable = value
    model1 = Model()
    model1.mutable = other
    session.add_all([model0, model1])
    session.commit()

    expected_error = error = None
    try:
        expected_result = func(value, other)
    except:
        expected_error = sys.exc_info()
    try:
        result = func(model0.mutable, model1.mutable)
    except:
        error = sys.exc_info()
    if expected_error is error is None:
        assert expected_result == result
    else:
        assert expected_error is not None and error is not None


def iadd(x, y):
    x += y
    return x


def isub(x, y):
    x -= y
    return x


def imul(x, y):
    x *= y
    return x


def ifloordiv(x, y):
    x //= y
    return x


def idiv(x, y):
    x /= y
    return x


def imod(x, y):
    x %= y
    return x


def ipow(x, y):
    x **= y
    return x


def iand(x, y):
    x &= y
    return x


def ior(x, y):
    x |= y
    return x


def ixor(x, y):
    x ^= y
    return x


inplace_functions = [
    iadd,
    isub,
    imul,
    ifloordiv,
    idiv,
    imod,
    ipow,
    iand,
    ior,
    ixor,
]


@pytest.mark.skipif(SKIP_INPLACE_FUNCTIONS, reason="skipping in-place functions")
@pytest.mark.parametrize(
    "value, other, func, other_is_mutable",
    product(values, values, inplace_functions, (True, False)),
)
def test_inplace_functions(value, other, func, other_is_mutable):
    # test functions that modify values in place
    print("value is", value, "other value is", other)
    model = Model()
    model.mutable = copy.deepcopy(value)
    session.add(model)
    session.commit()

    value = copy.deepcopy(value)
    other = copy.deepcopy(other)
    if other_is_mutable:
        other_model = Model()
        other_model.mutable = other

    expected_error = error = None
    try:
        value = func(value, other)
    except:
        expected_error = sys.exc_info()
    try:
        # note that the mutable object should track this change
        if other_is_mutable:
            func(model.mutable, other_model.mutable)
        else:
            func(model.mutable, other)
    except:
        error = sys.exc_info()
    if expected_error is error is None:
        session.commit()
        assert value == model.mutable
    else:
        assert expected_error is not None and error is not None


def foo(*args, **kwargs):
    return args, kwargs


def test_call():
    # test the call dunder method
    model = Model()
    model.mutable = foo
    session.add(model)
    session.commit()
    args = (1, 2, 3)
    kwargs = {"Hello": "World"}
    return_args, return_kwargs = model.mutable(*args, **kwargs)
    assert args == return_args and kwargs == return_kwargs


iter_values = ["Hello, world", [0, 1, 2], (0, 1, 2)]
iter_functions = [iter, reversed]


@pytest.mark.parametrize("value, func", product(iter_values, iter_functions))
def test_iteration(value, func):
    # test functions which operate on iterables
    model = Model()
    model.mutable = value
    session.add(model)
    session.commit()

    for i, j in zip(func(value), func(model.mutable)):
        assert i == j


def test_index():
    # test the __index__ method
    model = Model()
    values = [0, 1, 2]
    for index, value in enumerate(values):
        model.mutable = index
        assert values[model.mutable] == value


def test_format():
    # test the __format__ method
    model = Model()
    value = 1.123
    model.mutable = value
    assert f"{value:.1f}" == f"{model.mutable:.1f}"
    assert f"{value:.2f}" == f"{model.mutable:.2f}"
    assert f"{value:.3f}" == f"{model.mutable:.3f}"


def test_repr():
    model = Model()
    model.mutable = 0
    assert repr(model.mutable) == "<Mutable 0>"


def test_str():
    model = Model()
    model.mutable = 0
    assert str(model.mutable) == str(0)

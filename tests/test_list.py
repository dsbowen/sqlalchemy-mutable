import functools
import os

import pytest

if os.environ.get("FLASK_SETUP") == "1":
    from flask_setup import JSONModel, PickleModel, db

    session = db.session
else:
    from setup import JSONModel, PickleModel, session

default_list = [0, 1, 2]


def make_model_func(model_cls):
    model = model_cls()
    model.mutable = default_list.copy()
    session.add(model)
    session.commit()
    return model


@pytest.fixture(scope="module", params=(PickleModel, JSONModel))
def make_model(request):
    return functools.partial(make_model_func, model_cls=request.param)


def test_list_setting(make_model):
    model = make_model()
    assert model.mutable == default_list
    assert model.mutable.root is model.mutable
    assert 0 in model.mutable


@pytest.mark.parametrize("value", ([0, 1, 2], (0, 1, 2), {0, 1, 2}, "abc"))
def test_list_type_setting(make_model, value):
    model = make_model()
    model.mutable_list = value
    session.commit()
    assert model.mutable_list == list(value)

    with pytest.raises(TypeError, match="'*' object is not iterable"):
        model.mutable_list = 0


def test_setitem_tracking(make_model):
    model = make_model()
    model.mutable[-1] = 3
    session.commit()
    assert model.mutable[-1] == 3
    assert model.mutable.get_tracked_children() == []


def test_setitem_slice_tracking(make_model):
    model = make_model()
    model.mutable[1:] = [3, 4]
    session.commit()
    assert model.mutable[1:] == [3, 4]
    assert model.mutable.get_tracked_children() == []


def test_nested_item_tracking(make_model):
    model = make_model()

    model.mutable[-1] = [3, 4, 5]
    session.commit()
    assert model.mutable[-1] == [3, 4, 5]
    assert model.mutable[-1].root is model.mutable

    model.mutable[-1][0] = 2
    session.commit()
    assert model.mutable[-1] == [2, 4, 5]


def test_circular_item_tracking():
    # note that circular tracking doesn't work for JSON objects
    # because you can't JSON dump a circular list
    model = make_model_func(PickleModel)
    model.mutable.append(model.mutable)
    session.commit()
    assert model.mutable[-1] == model.mutable

    model.mutable.insert(0, -1)
    session.commit()
    assert model.mutable[-1][0] == -1


def test_del_item(make_model):
    model = make_model()
    del model.mutable[-1]
    session.commit()
    assert model.mutable == [0, 1]


def test_append(make_model):
    model = make_model()
    model.mutable.append(3)
    session.commit()
    assert model.mutable == [0, 1, 2, 3]


def test_clear(make_model):
    model = make_model()
    model.mutable.clear()
    session.commit()
    assert model.mutable == []


def test_copy(make_model):
    model = make_model()
    assert model.mutable.copy() == default_list


def test_count(make_model):
    model = make_model()
    model.mutable = [0, 0, 1]
    assert model.mutable.count(0) == 2
    assert model.mutable.count(1) == 1


def test_extend(make_model):
    model = make_model()
    model.mutable.extend([3, 4, 5])
    session.commit()
    assert model.mutable == [0, 1, 2, 3, 4, 5]


def test_index(make_model):
    model = make_model()
    for i in range(len(model.mutable)):
        assert model.mutable.index(i) == i


def test_insert(make_model):
    model = make_model()
    model.mutable.insert(0, -1)
    session.commit()
    assert model.mutable[0] == -1


def test_pop(make_model):
    model = make_model()
    value = model.mutable.pop()
    assert value == 2
    session.commit()
    assert model.mutable == [0, 1]


def test_remove(make_model):
    model = make_model()
    model.mutable.remove(2)
    session.commit()
    assert model.mutable == [0, 1]


def test_reverse(make_model):
    model = make_model()
    model.mutable.reverse()
    session.commit()
    assert model.mutable == [2, 1, 0]


def test_sort(make_model):
    model = make_model()
    model.mutable.sort(reverse=True)
    session.commit()
    assert model.mutable == [2, 1, 0]

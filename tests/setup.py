from sqlalchemy import create_engine, Column, Integer
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy_mutable import Query, Mutable
from sqlalchemy_mutable.types import *

engine = create_engine("sqlite:///:memory:")
session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)
session = Session()
Base = declarative_base()

Mutable.set_session(session)


class PickleModel(Base):
    __tablename__ = "pickle_model"
    id = Column(Integer, primary_key=True)
    mutable = Column(MutablePickleType)
    mutable_dict = Column(MutableDictPickleType)
    mutable_list = Column(MutableListPickleType)
    mutable_set = Column(MutableSetPickleType)
    mutable_tuple = Column(MutableTuplePickleType)
    query = Query(Session)


class JSONModel(Base):
    __tablename__ = "json_model"
    id = Column(Integer, primary_key=True)
    mutable = Column(MutableJSONType)
    mutable_dict = Column(MutableDictJSONType)
    mutable_list = Column(MutableListJSONType)
    mutable_tuple = Column(MutableTupleJSONType)
    html_settings = Column(HTMLSettingsType)
    query = Query(Session)

    def __init__(self):
        self.html_settings = {}


Base.metadata.create_all(engine)

import os

import pytest

if os.environ.get("FLASK_SETUP") == "1":
    from flask_setup import PickleModel as Model, db

    session = db.session
else:
    from setup import PickleModel as Model, session


def make_models():
    model0, model1 = Model(), Model()
    model0.mutable = model1
    session.add(model0)
    session.commit()
    return model0, model1


def test_model_setting():
    model0, model1 = make_models()
    assert model0.mutable == model1


def test_model_change_tracking():
    # any changes you make to model1 should be present in model0.mutable
    model0, model1 = make_models()
    model1.mutable = "Hello, world!"
    session.commit()
    assert model0.mutable.mutable == "Hello, world!"


def test_mutable_change_tracking():
    # any changes you make to model0.mutable should be present in model1
    model0, model1 = make_models()
    model0.mutable.mutable = "Hello, world!"
    session.commit()
    assert model1.mutable == "Hello, world!"

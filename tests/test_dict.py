import functools
import os

import pytest

if os.environ.get("FLASK_SETUP") == "1":
    from flask_setup import JSONModel, PickleModel, db

    session = db.session
else:
    from setup import JSONModel, PickleModel, session

default_dict = {"Hello": "World"}


def make_model_func(model_cls):
    model = model_cls()
    model.mutable = default_dict.copy()
    session.add(model)
    session.commit()
    return model


@pytest.fixture(scope="module", params=(PickleModel, JSONModel))
def make_model(request):
    return functools.partial(make_model_func, model_cls=request.param)


def test_dict_setting(make_model):
    model = make_model()
    assert model.mutable == default_dict
    assert model.mutable.root is model.mutable
    assert "Hello" in model.mutable


@pytest.mark.parametrize("value", ({"Hello": "World"}, [("Hello", "World")]))
def test_dict_type_setting(make_model, value):
    model = make_model()
    model.mutable_dict = value
    session.commit()
    assert model.mutable_dict == dict(value)

    with pytest.raises(TypeError, match="'*' object is not iterable"):
        model.mutable_dict = 0


def test_setitem_tracking(make_model):
    model = make_model()
    model.mutable["Hello"] = "Moon"
    session.commit()
    assert model.mutable["Hello"] == "Moon"


def test_nested_item_tracking(make_model):
    model = make_model()

    model.mutable["Goodbye"] = {"Seeya": "Moon"}
    session.commit()
    assert model.mutable["Goodbye"] == {"Seeya": "Moon"}
    assert model.mutable["Goodbye"].root is model.mutable

    model.mutable["Goodbye"]["Seeya"] = "Sun"
    session.commit()
    assert model.mutable["Goodbye"] == {"Seeya": "Sun"}


def test_circular_item_tracking():
    # note that circular tracking doesn't work for JSON objects
    # because you can't JSON dump a circular dict
    model = make_model_func(PickleModel)
    model.mutable["Goodbye"] = model.mutable
    session.commit()
    assert model.mutable["Goodbye"] == model.mutable

    model.mutable["Seeya"] = "Moon"
    session.commit()
    assert model.mutable["Goodbye"]["Seeya"] == "Moon"


def test_del_item(make_model):
    model = make_model()
    del model.mutable["Hello"]
    session.commit()
    assert "Hello" not in model.mutable
    assert model.mutable.get_tracked_children() == []


def test_clear(make_model):
    model = make_model()
    model.mutable.clear()
    session.commit()
    assert model.mutable == {}


def test_get(make_model):
    model = make_model()
    assert model.mutable.get("Hello") == "World"
    assert model.mutable.get("Goodbye") is None


def test_items(make_model):
    model = make_model()
    assert model.mutable.items() == default_dict.items()


def test_keys(make_model):
    model = make_model()
    assert model.mutable.keys() == default_dict.keys()


def test_pop(make_model):
    model = make_model()
    value = model.mutable.pop("Hello")
    session.commit()
    assert value == "World"
    assert model.mutable == {}


def test_popitem(make_model):
    model = make_model()
    value = model.mutable.popitem()
    session.commit()
    assert value == default_dict.copy().popitem()
    assert model.mutable == {}


def test_setdefault(make_model):
    model = make_model()
    model.mutable.setdefault("Goodbye", "Moon")
    session.commit()
    assert model.mutable["Goodbye"] == "Moon"
    model.mutable["Goodbye"] = "Sun"
    assert model.mutable["Goodbye"] == "Sun"


def test_update(make_model):
    model = make_model()
    model.mutable.update({"Goodbye": "Moon"})
    session.commit()
    assert model.mutable["Goodbye"] == "Moon"


def test_values(make_model):
    model = make_model()
    assert type(model.mutable.values()) == type(default_dict.values())
    for m_value, value in zip(model.mutable.values(), default_dict.values()):
        assert m_value == value

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from sqlalchemy_mutable import Mutable
from sqlalchemy_mutable.types import *

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///:memory:"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)

Mutable.set_session(db.session)


class PickleModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    mutable = db.Column(MutablePickleType)
    mutable_dict = db.Column(MutableDictPickleType)
    mutable_list = db.Column(MutableListPickleType)
    mutable_set = db.Column(MutableSetPickleType)
    mutable_tuple = db.Column(MutableTuplePickleType)


class JSONModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    mutable = db.Column(MutableJSONType)
    mutable_dict = db.Column(MutableDictJSONType)
    mutable_list = db.Column(MutableListJSONType)
    mutable_tuple = db.Column(MutableTupleJSONType)
    html_settings = db.Column(HTMLSettingsType)

    def __init__(self):
        self.html_settings = {}


db.create_all()
session = db.session

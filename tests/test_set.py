import os

import pytest

from sqlalchemy_mutable import Mutable

if os.environ.get("FLASK_SETUP") == "1":
    from flask_setup import PickleModel as Model, db

    session = db.session
else:
    from setup import PickleModel as Model, session

default_set = {0, 1, 2}


@pytest.fixture(scope="module", params=(True, False))
def item_is_mutable(request):
    return request.param


def make_model():
    model = Model()
    model.mutable = default_set.copy()
    session.add(model)
    session.commit()
    return model


def test_set_setting():
    model = make_model()
    assert model.mutable == default_set
    assert model.mutable.root is model.mutable
    assert 0 in model.mutable


@pytest.mark.parametrize("value", ([0, 1, 2], (0, 1, 2), {0, 1, 2}, "abc"))
def test_set_type_setting(value):
    model = make_model()
    model.mutable_set = value
    session.commit()
    assert model.mutable_set == set(value)

    with pytest.raises(TypeError, match="'*' object is not iterable"):
        model.mutable_set = 0


def test_add(item_is_mutable):
    model = make_model()
    item = 3
    if item_is_mutable:
        item = Mutable(item)
    model.mutable.add(Mutable(3) if item_is_mutable else 3)
    session.commit()
    assert model.mutable == {0, 1, 2, 3}


def test_clear():
    model = make_model()
    model.mutable.clear()
    session.commit()
    assert model.mutable == set()


def test_copy():
    model = make_model()
    assert model.mutable.copy() == default_set


def test_difference():
    model = make_model()
    other = {2, 3, 4}
    assert model.mutable.difference(other) == default_set.difference(other)


def test_difference_update():
    model = make_model()
    other = {2, 3, 4}
    expected_result = default_set.copy()
    expected_result.difference_update(other)
    model.mutable.difference_update(other)
    session.commit()
    assert model.mutable == expected_result


def test_discard(item_is_mutable):
    model = make_model()
    item = 2
    if item_is_mutable:
        item = Mutable(item)
    model.mutable.discard(item)
    session.commit()
    assert model.mutable == {0, 1}


def test_intersection():
    model = make_model()
    other = {2, 3, 4}
    assert model.mutable.intersection(other) == default_set.intersection(other)


def test_intersection_update():
    model = make_model()
    other = {2, 3, 4}
    expected_result = default_set.copy()
    expected_result.intersection_update(other)
    model.mutable.intersection_update(other)
    session.commit()
    assert model.mutable == expected_result


def test_pop():
    model = make_model()
    result = model.mutable.pop()
    assert result in default_set
    session.commit()
    assert len(model.mutable) == len(default_set) - 1


def test_remove(item_is_mutable):
    model = make_model()
    item = 2
    if item_is_mutable:
        item = Mutable(item)
    model.mutable.remove(item)
    session.commit()
    assert model.mutable == {0, 1}


def test_symmetric_difference():
    model = make_model()
    other = {2, 3, 4}
    assert model.mutable.symmetric_difference(
        other
    ) == default_set.symmetric_difference(other)


def test_symmetric_difference_update():
    model = make_model()
    other = {2, 3, 4}
    expected_result = default_set.copy()
    expected_result.symmetric_difference_update(other)
    model.mutable.symmetric_difference_update(other)
    session.commit()
    assert model.mutable == expected_result


def test_union():
    model = make_model()
    other = {2, 3, 4}
    assert model.mutable.union(other) == default_set.union(other)


def test_update():
    model = make_model()
    other = {2, 3, 4}
    expected_result = default_set.copy()
    expected_result.update(other)
    model.mutable.update(other)
    session.commit()
    assert model.mutable == expected_result

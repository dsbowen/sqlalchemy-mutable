import os

import pytest

if os.environ.get("FLASK_SETUP") == "1":
    from flask_setup import JSONModel as Model, session
else:
    from setup import JSONModel as Model, session


input_attributes = {
    "class": ["form-control", "w-100"],
    "style": {"background-color": "black"},
    "type": "text",
    "required": True,
    "other_attribute0": False,
    "other_attribute1": None,
}


def test_html_attribute():
    model = Model()
    model.html_settings["input"] = input_attributes
    session.add(model)
    session.commit()
    assert (
        model.html_settings["input"].get_attrs()
        == 'class="form-control w-100" style="background-color:black;" type="text" required'
    )


def test_html_attribute_with_0():
    model = Model()
    model.html_settings["input"] = {"min": 0}
    session.add(model)
    session.commit()
    assert model.html_settings["input"].get_attrs() == 'min="0"'


def test_empty_css_js():
    model = Model()
    assert not model.html_settings.get_css()
    assert not model.html_settings.get_js()


class TestUpdate:
    def test_html_attribute(self):
        model = Model()
        model.html_settings["input"] = input_attributes
        session.add(model)
        session.commit()

        model.html_settings.update_settings(
            {
                "input": {
                    "class": ["new-class"],
                    "style": {"color": "white"},
                    "required": False,
                }
            }
        )
        session.commit()

        assert model.html_settings["input"]["class"] == [
            "form-control",
            "w-100",
            "new-class",
        ]
        assert model.html_settings["input"]["style"] == {
            "background-color": "black",
            "color": "white",
        }
        assert model.html_settings["input"]["required"] == False

    def test_update_from_empty(self):
        model = Model()
        model.html_settings["input"] = {}
        session.add(model)
        session.commit()

        model.html_settings.update_settings(
            {
                "input": {
                    "class": ["form-control"],
                    "style": {"background-color": "black"},
                },
                "h1": {"class": ["w-100"]},
            }
        )
        session.commit()

        assert model.html_settings["input"]["class"] == ["form-control"]
        assert model.html_settings["input"]["style"] == {"background-color": "black"}
        assert model.html_settings["h1"] == {"class": ["w-100"]}

    @pytest.mark.parametrize("as_list", (True, False))
    def test_css(self, as_list):
        old_css, new_css = "https://my-css.html", "https://my-new-css.html"
        model = Model()
        model.html_settings["css"] = old_css
        session.add(model)
        session.commit()

        model.html_settings.update_settings({"css": [new_css] if as_list else new_css})
        session.commit()

        assert model.html_settings["css"] == [old_css, new_css]

    @pytest.mark.parametrize("as_list", (True, False))
    def test_js(self, as_list):
        old_js, new_js = "https://my-js.html", "https://my-new-js.html"
        model = Model()
        model.html_settings["js"] = {"src": old_js}
        session.add(model)
        session.commit()

        update = {"src": new_js}
        model.html_settings.update_settings({"js": [update] if as_list else update})
        session.commit()

        assert model.html_settings["js"] == [{"src": old_js}, {"src": new_js}]


class TestCSS:
    def test_from_href(self):
        href = "https://my-css.html"
        model = Model()
        model.html_settings["css"] = href
        session.add(model)
        session.commit()
        assert model.html_settings.get_css() == f'<link rel="stylesheet" href="{href}">'

    def test_from_link_tag(self):
        link_tag = '<link rel="stylesheet" href="https://my-css.html">'
        model = Model()
        model.html_settings["css"] = link_tag
        session.add(model)
        session.commit()
        assert model.html_settings.get_css() == link_tag

    def test_from_internal_css(self):
        model = Model()
        model.html_settings["css"] = {
            "body": {"background-color": "lightblue"},
            "h1": {"color": "white", "text-align": "center"},
        }
        session.add(model)
        session.commit()
        assert (
            model.html_settings.get_css()
            == "<style>body{background-color:lightblue;}h1{color:white;text-align:center;}</style>"
        )


class TestJS:
    def test_from_raw_js(self):
        js = "$(document).ready(function(){alert('hello world');});"
        model = Model()
        model.html_settings["js"] = js
        session.add(model)
        session.commit()
        assert model.html_settings.get_js() == f"<script>{js}</script>"

    def test_from_script_tag(self):
        js = "<script>$(document).ready(function(){alert('hello world');});</script>"
        model = Model()
        model.html_settings["js"] = js
        session.add(model)
        session.commit()
        assert model.html_settings.get_js() == js

    def test_from_src(self):
        src = "https://my-js.html"
        model = Model()
        model.html_settings["js"] = {"src": src}
        session.add(model)
        session.commit()
        assert model.html_settings.get_js() == f'<script src="{src}"></script>'

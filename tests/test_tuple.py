import copy
import os
from itertools import product

import pytest

if os.environ.get("FLASK_SETUP") == "1":
    from flask_setup import JSONModel, PickleModel, db

    session = db.session
else:
    from setup import JSONModel, PickleModel, session

default_tuple = (0, 1, 2)


def make_model(model_cls=PickleModel):
    model = model_cls()
    model.mutable = copy.copy(default_tuple)
    session.add(model)
    session.commit()
    return model


def test_tuple_setting():
    model = make_model()
    assert model.mutable == default_tuple
    assert 0 in model.mutable


@pytest.mark.parametrize(
    "model_cls, value",
    product((JSONModel, PickleModel), ((0, 1, 2), [0, 1, 2], {0, 1, 2}, "abc")),
)
def test_tuple_type_setting(model_cls, value):
    model = make_model(model_cls)
    model.mutable_tuple = value
    session.commit()
    assert model.mutable_tuple == tuple(value)

    with pytest.raises(TypeError, match="'*' object is not iterable"):
        model.mutable_tuple = 0


def test_nested_item_tracking():
    model = make_model()
    model.mutable = (0, 1, [2, 3])
    session.commit()
    model.mutable[-1].append(4)
    session.commit()
    assert model.mutable[-1] == [2, 3, 4]


def test_copy():
    model = make_model()
    model.mutable = (0, 0, 1)
    assert model.mutable.count(0) == 2
    assert model.mutable.count(1) == 1


def test_index():
    model = make_model()
    for i in range(len(model.mutable)):
        assert model.mutable.index(i) == i

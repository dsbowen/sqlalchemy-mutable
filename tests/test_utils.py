import os
from itertools import product

import pytest

from sqlalchemy_mutable import Mutable
from sqlalchemy_mutable.utils import (
    get_object,
    is_callable,
    is_instance,
    is_subclass,
    partial,
)

if os.environ.get("FLASK_SETUP") == "1":
    from flask_setup import PickleModel as Model, db

    session = db.session
else:
    from setup import PickleModel as Model, session


@pytest.mark.parametrize("as_mutable", (True, False))
def test_get_object(as_mutable):
    obj = "hello, world"
    new_obj = Mutable(obj) if as_mutable else obj
    assert get_object(new_obj) is obj


def foo():
    pass


@pytest.mark.parametrize("obj, as_mutable", product(("hello", foo), (True, False)))
def test_is_callable(obj, as_mutable):
    new_obj = Mutable(obj) if as_mutable else obj
    assert callable(obj) is is_callable(new_obj)


class Point:
    def __init__(self, x):
        self.x = x

    def __eq__(self, other):
        return is_instance(other, Point) and self.x == other.x


def test_is_instance():
    assert is_instance(Mutable(Point(0)), Point)
    assert is_instance(Point(0), Point)


def test_is_subclass():
    assert is_subclass(Mutable(Point), Point)
    assert is_subclass(Point, Point)


def foo(name, friend_name):
    return f"{name} is friends with {friend_name}"


def test_partial():
    name, friend_name = "World", "Moon"
    expected_output = foo(name, friend_name)
    model = Model()

    model.mutable = partial(foo, name, friend_name)
    assert model.mutable() == expected_output

    session.commit()
    assert model.mutable() == expected_output

    model.mutable = partial(foo, name, friend_name=friend_name)
    session.commit()
    assert model.mutable() == expected_output

    model.mutable = partial(foo, name=name, friend_name=friend_name)
    session.commit()
    assert model.mutable() == expected_output


class Person:
    def __init__(self, name):
        self.name = name


def bar(model):
    return model.mutable.name


@pytest.mark.parametrize("as_keyword", (True, False))
def test_partial_with_model(as_keyword):
    # test partial with a model as an argument
    model0 = Model()
    session.add(model0)
    session.commit()

    model1 = Model()
    model0.mutable = partial(bar, model=model1) if as_keyword else partial(bar, model1)
    session.commit()
    model1.mutable = Person("World")
    assert model0.mutable() == "World"

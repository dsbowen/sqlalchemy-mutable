black
build
coverage
ipykernel
jupyter
mypy
nbsphinx
pandoc
pydata-sphinx-theme
pylint
sphinx
tox

flask-sqlalchemy
